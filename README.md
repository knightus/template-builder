# Template Builder
This application is built for building template settings. Users can add pre-defined components & clone/remove/edit settings by their own will. Exported settings can be used in generate HTML themes/pages like carousel with/without auto-play, but are independent from HTML structure (2 themes can share the same settings).

I build this application using edge technology like ReactJS + Redux, webpack for education purpose. But hope to use it as an ultimate Frontend Tool for both personal & at-work use once it is finished.

##### Setting categories
The settings are divided into two categories: Presentational & Content.

`Ex:` A carousel will have presentational settings like `backgroundColor`, `slide speed`, `autoPlay`, `showTitle` and content ones like its `images`.

##### Import/export data
The application receives nested data, then **normalize** it while using. The exported data will be **denormalized** to its previous scheme

##### Current State
I'm refactoring this code base so just collapse/expand function is implemented in current version (click on caret icon at the top right corner of each component).

##### Todos
Write reducers for remain actions (reference in `constants/action-types`)

# Prerequisites
- `nodejs>=4.4.3`
- `npm>=3.8.7`
- `npm i -g gulp`

# Installation Notes
- `npm i`

# Usage
- Run one of these commands:
    - Compile: `node webpack`
    - Compile & watch for changes: `node webpack:watch`
    - Run webpack dev server: `node webpack-dev-server`
    - Run webpack dev server & watch for changes: `node webpack-dev-server:watch`
- Navigate to `localhost:9012`

# Technologies
This application uses a number of open source projects to work properly:

- **[tool]** `Webpack`
- **[JS]** `ReactJS` + `Redux` + `normalizr`
- **[JS]** `lodash`
- **[JS]** `jQuery#2.2.4`
- **[JS]** `bows` (use as logger)
- **[CSS]** `LESS`
- **[CSS]** `bootstrap#3.3.6`
- **[CSS]** `font-awesome`
- **[HTML]** `swig`
- **[convention]** `mern.io` (for code structure convention)
- **[convention]** `semantic-UI` (for naming convention)
- **[convention]** `BEM` (for naming convention)
- **[convention]** `ITCSS` (for CSS code structure)

# Code Structure

##### Styles
- `overrides`: Include third-party libraries and tweak theirs styles/variables
- `settings`: Include basic styles for fonts, typography, colors...
- `layouts`: Styles for main layouts
- `components`: Styles for small components such as buttons, icons, ...
- `modules`: Styles for modules, which are formed by smaller components
- `pages`: Styles for specific pages

##### Scripts
- `index.jsx`: Entry point
- `constants`: constants for using across whole application
- `pages`: script for specific pages
- `actions`: definitions of Redux actions
- `reducers`: definitions of Redux reducers
- `stores`: handle data receive from 
- `components`: Store ReactJS Components (both smart & dump)

##### Templates
- `pages`: HTML Template for specific pages, can be extended from `layouts`
- `layouts`: HTML Layouts (temporarily not use)
- `modules`: HTML modules that can be used across pages like `header`, `footer` (temporarily not use)