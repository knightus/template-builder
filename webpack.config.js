var webpack = require('webpack');
var path = require('path');

var HtmlWebpackPlugin = require('html-webpack-plugin');
var WebpackNotifierPlugin = require('webpack-notifier');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var BUILD_DIR = path.resolve(__dirname, 'build');
var APP_DIR = path.resolve(__dirname, 'source');

var config = {
    entry: {
        main: APP_DIR + '/scripts/index.jsx',
        iframe: APP_DIR + '/scripts/pages/iframe.js'
    },
    output: {
        path: BUILD_DIR,
        filename: 'scripts/[name].js?hash=[chunkhash]',
    },
    module : {
        loaders : [
            {
                test : /\.jsx?/,
                exclude: /node_modules/,
                loader : 'babel',
                query: {
                    cacheDirectory: true,
                    presets: ['react', 'es2015']
                }
            }, {
                test : /\.css$/,
                loader : ExtractTextPlugin.extract("style", "css")
            }, {
                test : /\.less$/,
                loader : ExtractTextPlugin.extract("style", "css!less")
            }, {
                test : /\.(woff|woff2|eot|ttf|svg)(\?.*$|$)$/,
                loader : 'file-loader?name=/assets/fonts/[name].[hash].[ext]'
            }, {
                test : /\.(gif|jpg|png)(\?.*$|$)$/,
                loader : 'file-loader?name=/assets/images/[name].[hash].[ext]'
            }, {
                test : /jquery\/src\/selector\.js$/,
                loader : 'amd-define-factory-patcher-loader'
            }
        ]
    },
    resolve: {
        modulesDirectories: ["node_modules","./client/source/vendors"],
        extensions: ['', '.js', '.jsx']
    },
    plugins : [
        new HtmlWebpackPlugin({
            title : 'ADR Template Builder',
            filename : 'index.html',
            template : APP_DIR + '/templates/pages/index.swig',
            inject : true
        }),
        new HtmlWebpackPlugin({
            title : 'ADR Template Builder',
            filename : 'iframe.html',
            template : APP_DIR + '/templates/pages/iframe.swig',
            inject: true,
            files: {
                js: [ BUILD_DIR +  'scripts/iframe.js']
            }
        }),
        new WebpackNotifierPlugin(),
        new ExtractTextPlugin("./styles/[name].css?[chunkhash]", {
            allChunks: true
        }),
        /*new webpack.ProvidePlugin({
         $ : "jquery",
         jQuery : "jquery",
         "window.jQuery": "jquery"
         }),*/
        new webpack.ResolverPlugin([
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"]),
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin(".bower.json", ["main"])
        ],["normal", "loader"])
    ],
    devServer: {
        inline:true,
        port: 9012
    },
};

module.exports = config;