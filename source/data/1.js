'use strict';

/**
 * Template Data - VinDS - Shoe Center
 */

export default {
    "id": 1,
    "version": 0.1,
    "createdTime": "2016-04-18 10:13:56",
    "type": "Template",
    "childComponents": [
        {
            "id": 2,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 3,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 4,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 5,
                                    "type": "Component.bannersGroup1",
                                    "properties": {
                                        "container": {
                                            "id": 101
                                        },
                                        "presentational": {
                                            "type": "carousel",
                                            "isFullWidth": true,
                                            "carousel": {
                                                "singleItem": true,
                                                "slideSpeed": 300,
                                                "paginationSpeed": 400,
                                                "navigation": true,
                                                "autoPlay": true,
                                                "stopOnHover": true,
                                                "navigationText": false,
                                                "autoHeight": true
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 6,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 7,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 8,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 9,
                                    "type": "Component.bannersGroup2",
                                    "properties": {
                                        "container": {
                                            "id": 102
                                        },
                                        "presentational": {
                                            "height": "600px",
                                            "style": {
                                                "childComponents": {
                                                    "stateHover": {
                                                        "opacity": 0.6
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "childComponents": [
                                        {
                                            "id": 10,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": 0,
                                                        "left": 0,
                                                        "width": "400px",
                                                        "height": "600px"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            "id": 11,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": 0,
                                                        "left": "420px",
                                                        "width": "400px",
                                                        "height": "290px"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            "id": 12,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": "310px",
                                                        "left": "420px",
                                                        "width": "400px",
                                                        "height": "290px"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            "id": 13,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": 0,
                                                        "left": "840px",
                                                        "width": "400px",
                                                        "height": "600px"
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 14,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 15,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 16,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 17,
                                    "type": "Component.bannersGroup1",
                                    "properties": {
                                        "container": {
                                            "id": 103
                                        },
                                        "presentational": {
                                            "type": "carousel",
                                            "isFullWidth": false,
                                            "carousel": {
                                                "autoPlay": false,
                                                "items": 4,
                                                "scrollPerPage": true,
                                                "slideSpeed": 300,
                                                "paginationSpeed": 400,
                                                "navigation": true,
                                                "pagination": false,
                                                "stopOnHover": true,
                                                "navigationText": false
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 18,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 19,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 20,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 21,
                                    "type": "Component.GroupBanner1",
                                    "properties": {
                                        "container": {
                                            "id": 104
                                        },
                                        "presentational": {
                                            "type": "static",
                                            "title": "Đang giảm giá",
                                            "isFullWidth": false,
                                            "static": {
                                                "items": 4
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 22,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 23,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 24,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 25,
                                    "type": "Component.bannersGroup1",
                                    "properties": {
                                        "container": {
                                            "id": 105
                                        },
                                        "presentational": {
                                            "type": "static",
                                            "isFullWidth": true,
                                            "backgroundImage": "https://www.adayroi.com/build/assets/images/demo/pages/vinds.shoe-center/zone-4.background.jpg",
                                            "static": {
                                                "items": 2
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 26,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 27,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 28,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 29,
                                    "type": "Component.ProductsList",
                                    "properties": {
                                        "container": {
                                            "id": 4,
                                            "items": 8
                                        },
                                        "presentational": {
                                            "title": "Đang giảm giá"
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
};