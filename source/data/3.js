'use strict';

/**
 * Template Data - VinDS - Beauty Zone
 */

export default {
    "id": 1,
    "version": 0.1,
    "createdTime": "2016-04-18 10:13:56",
    "type": "Template",
    "childComponents": [
        {
            "id": 2,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 3,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 4,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 5,
                                    "type": "Component.BannersGroup1",
                                    "properties": {
                                        "container": {
                                            "id": 89
                                        },
                                        "presentational": {
                                            "type": "carousel",
                                            "isFullWidth": true,
                                            "carousel": {
                                                "singleItem": true,
                                                "slideSpeed": 300,
                                                "paginationSpeed": 400,
                                                "navigation": true,
                                                "autoPlay": true,
                                                "stopOnHover": true,
                                                "navigationText": false,
                                                "autoHeight": true
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 6,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 7,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 8,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 9,
                                    "type": "Component.bannersGroup2",
                                    "properties": {
                                        "container": {
                                            "id": 102
                                        },
                                        "presentational": {
                                            "width": "944px",
                                            "height": "600px",
                                            "style": {
                                                "childComponents": {
                                                    "stateHover": {
                                                        "opacity": 0.6
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "childComponents": [
                                        {
                                            "id": 10,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": 0,
                                                        "left": 0,
                                                        "width": "304px",
                                                        "height": "528px"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            "id": 11,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": "0",
                                                        "left": "320px",
                                                        "width": "304px",
                                                        "height": "304px"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            "id": 12,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": "328px",
                                                        "left": "320px",
                                                        "width": "304px",
                                                        "height": "304px"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            "id": 13,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": "0px",
                                                        "left": "640px",
                                                        "width": "304px",
                                                        "height": "592px"
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 14,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 15,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 16,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 17,
                                    "type": "Component.GroupBanner2",
                                    "properties": {
                                        "container": {
                                            "id": 104
                                        },
                                        "presentational": {
                                            "title": "Bộ sưu tập mới",
                                            "subtitle": "BE DAZZLED BY THE BEAUTY YOU CAN FEEL",
                                            "height": "336px",
                                            "style": {
                                                "childComponents": {
                                                    "stateHover": {
                                                        "opacity": 0.6
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "childComponents": [
                                        {
                                            "id": 18,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": 0,
                                                        "left": 0,
                                                        "width": "775px",
                                                        "height": "272px"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            "id": 19,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": "0",
                                                        "left": "850px",
                                                        "width": "352px",
                                                        "height": "120px"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            "id": 20,
                                            "type": "Component.bannersGroup2.Item",
                                            "properties": {
                                                "presentational": {
                                                    "style": {
                                                        "top": "152px",
                                                        "left": "850px",
                                                        "width": "352px",
                                                        "height": "120px"
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
};