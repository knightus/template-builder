'use strict';

/**
 * Template Data - VinMart
 */

export default {
    "id": 1,
    "version": 0.1,
    "createdTime": "2016-04-18 10:13:56",
    "type": "Template",
    "childComponents": [
        {
            "id": 2,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 3,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 4,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 5,
                                    "type": "Component.BannersGroup1",
                                    "properties": {
                                        "container": {
                                            "id": 282
                                        },
                                        "presentational": {
                                            "type": "carousel",
                                            "isFullWidth": false,
                                            "carousel": {
                                                "singleItem": true,
                                                "slideSpeed": 300,
                                                "paginationSpeed": 400,
                                                "navigation": true,
                                                "autoPlay": true,
                                                "stopOnHover": true,
                                                "navigationText": false,
                                                "autoHeight": true,
                                                "showTitle": true
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 6,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 7,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 8,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 9,
                                    "type": "Component.BannersGroup1",
                                    "properties": {
                                        "container": {
                                            "id": 283
                                        },
                                        "presentational": {
                                            "type": "static",
                                            "isFullWidth": false
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 10,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 11,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 12,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 13,
                                    "type": "Component.ProductsList",
                                    "properties": {
                                        "container": {
                                            "id": 34
                                        },
                                        "presentational": {
                                            "type": "carousel",
                                            "carousel": {
                                                "library": "slick",
                                                "infinite": false,
                                                "dots": false,
                                                "slidesToShow": 6,
                                                "slidesToScroll": 5,
                                                "prevArrow": "<span class=\"slick-prev\"></span>",
                                                "nextArrow": "<span class=\"slick-next\"></span>",
                                                "responsive": [
                                                    {
                                                        "breakpoint": 1270,
                                                        "settings": {
                                                            "slidesToShow": 5,
                                                            "slidesToScroll": 4,
                                                            "infinite": false
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 14,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 15,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 16,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 17,
                                    "type": "Component.ProductsList",
                                    "properties": {
                                        "container": {
                                            "id": 284
                                        },
                                        "presentational": {
                                            "type": "grid",
                                            "itemsPerColumn": 3
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 18,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 19,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 20,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 21,
                                    "type": "Component.ProductsList",
                                    "properties": {
                                        "container": {
                                            "id": 35
                                        },
                                        "presentational": {
                                            "type": "carousel",
                                            "carousel": {
                                                "items": 4,
                                                "itemsDesktop": [1270, 3],
                                                "slideSpeed": 300,
                                                "paginationSpeed": 400,
                                                "navigation": true,
                                                "pagination": false,
                                                "stopOnHover": true,
                                                "navigationText": false,
                                                "autoHeight": true
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}