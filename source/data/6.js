'use strict';

/**
 * Template Data - VinPro
 */

export default {
    "id": 1,
    "version": 0.1,
    "createdTime": "2016-04-18 10:13:56",
    "type": "Template",
    "childComponents": [
        {
            "id": 2,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 3,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 4,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 5,
                                    "type": "Component.HorizontalTab",
                                    "properties": {
                                        "presentational": {}
                                    },
                                    "childComponents": [
                                        {
                                            "id": 6,
                                            "type": "Component.HorizontalTab.Item",
                                            "properties": {},
                                            "childComponents": [
                                                {
                                                    "id": 7,
                                                    "type": "Component.ProductsList",
                                                    "properties": {
                                                        "container": {
                                                            "id": 52,
                                                            "limit": 8
                                                        },
                                                        "presentational": {
                                                            "type": "carousel",
                                                            "carousel": {
                                                                "autoPlay": false,
                                                                "items": 4,
                                                                "itemsCustom": [[0, 3], [1270, 4]],
                                                                "scrollPerPage": true,
                                                                "slideSpeed": 300,
                                                                "paginationSpeed": 400,
                                                                "navigation": true,
                                                                "pagination": false,
                                                                "stopOnHover": true,
                                                                "navigationText": false
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            "id": 8,
                                            "type": "Component.HorizontalTab.Item",
                                            "properties": {},
                                            "childComponents": [
                                                {
                                                    "id": 9,
                                                    "type": "Component.ProductsList",
                                                    "properties": {
                                                        "container": {
                                                            "id": 40,
                                                            "limit": 8
                                                        },
                                                        "presentational": {
                                                            "type": "carousel",
                                                            "carousel": {
                                                                "autoPlay": false,
                                                                "items": 4,
                                                                "itemsCustom": [[0, 3], [1270, 4]],
                                                                "scrollPerPage": true,
                                                                "slideSpeed": 300,
                                                                "paginationSpeed": 400,
                                                                "navigation": true,
                                                                "pagination": false,
                                                                "stopOnHover": true,
                                                                "navigationText": false
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            "id": 10,
                                            "type": "Component.HorizontalTab.Item",
                                            "properties": {},
                                            "childComponents": [
                                                {
                                                    "id": 11,
                                                    "type": "Component.ProductsList",
                                                    "properties": {
                                                        "container": {
                                                            "id": 41,
                                                            "limit": 8
                                                        },
                                                        "presentational": {
                                                            "type": "carousel",
                                                            "carousel": {
                                                                "autoPlay": false,
                                                                "items": 4,
                                                                "itemsCustom": [[0, 3], [1270, 4]],
                                                                "scrollPerPage": true,
                                                                "slideSpeed": 300,
                                                                "paginationSpeed": 400,
                                                                "navigation": true,
                                                                "pagination": false,
                                                                "stopOnHover": true,
                                                                "navigationText": false
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            "id": 12,
                                            "type": "Component.HorizontalTab.Item",
                                            "properties": {},
                                            "childComponents": [
                                                {
                                                    "id": 13,
                                                    "type": "Component.ProductsList",
                                                    "properties": {
                                                        "container": {
                                                            "id": 42,
                                                            "limit": 8
                                                        },
                                                        "presentational": {
                                                            "type": "carousel",
                                                            "carousel": {
                                                                "autoPlay": false,
                                                                "items": 4,
                                                                "itemsCustom": [[0, 3], [1270, 4]],
                                                                "scrollPerPage": true,
                                                                "slideSpeed": 300,
                                                                "paginationSpeed": 400,
                                                                "navigation": true,
                                                                "pagination": false,
                                                                "stopOnHover": true,
                                                                "navigationText": false
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            "id": 14,
                                            "type": "Component.HorizontalTab.Item",
                                            "properties": {},
                                            "childComponents": [
                                                {
                                                    "id": 15,
                                                    "type": "Component.ProductsList",
                                                    "properties": {
                                                        "container": {
                                                            "id": 43,
                                                            "limit": 8
                                                        },
                                                        "presentational": {
                                                            "type": "carousel",
                                                            "carousel": {
                                                                "autoPlay": false,
                                                                "items": 4,
                                                                "itemsCustom": [[0, 3], [1270, 4]],
                                                                "scrollPerPage": true,
                                                                "slideSpeed": 300,
                                                                "paginationSpeed": 400,
                                                                "navigation": true,
                                                                "pagination": false,
                                                                "stopOnHover": true,
                                                                "navigationText": false
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            "id": 16,
                                            "type": "Component.HorizontalTab.Item",
                                            "properties": {},
                                            "childComponents": [
                                                {
                                                    "id": 17,
                                                    "type": "Component.ProductsList",
                                                    "properties": {
                                                        "container": {
                                                            "id": 44,
                                                            "limit": 8
                                                        },
                                                        "presentational": {
                                                            "type": "carousel",
                                                            "carousel": {
                                                                "autoPlay": false,
                                                                "items": 4,
                                                                "itemsCustom": [[0, 3], [1270, 4]],
                                                                "scrollPerPage": true,
                                                                "slideSpeed": 300,
                                                                "paginationSpeed": 400,
                                                                "navigation": true,
                                                                "pagination": false,
                                                                "stopOnHover": true,
                                                                "navigationText": false
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]

                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 18,
            "type": "Component.Grid.Container",
            "childComponents": [
                {
                    "id": 19,
                    "type": "Component.Grid.Row",
                    "childComponents": [
                        {
                            "id": 20,
                            "type": "Component.Grid.Column",
                            "childComponents": [
                                {
                                    "id": 21,
                                    "type": "Component.Complex1",
                                    "properties": {
                                        "container": {
                                            "menu": {
                                                "id": 1
                                            },
                                            "productsList": {
                                                "id": 45,
                                                "limit": 6
                                            },
                                            "bannersGroup": {
                                                "id": 297
                                            }
                                        },
                                        "presentational": {}
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}