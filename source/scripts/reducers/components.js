import * as ACTION_TYPES from './../constants/action-types';

import _ from 'lodash';

var log = require('bows/bows.js')('reducers/components');

const initialState = {
    version: '0.1',
    id: 0,
    type: 'Template',
    childComponents: []
}

export default function components(state = initialState, action){
    var payload = action.payload;
    switch(action.type){
        case ACTION_TYPES.COMPONENT_TOGGLE_COLLAPSE:
            //clone current state
            var nextState = _.cloneDeep(state);

            nextState[payload.id].isCollapsed = !nextState[payload.id].isCollapsed;

            return nextState;
            break;

        case ACTION_TYPES.COMPONENT_TOGGLE_COLLAPSE_ALL:
            var nextState = _.cloneDeep(state);

            return _.forEach(nextState, function(component){
                return component.isCollapsed = true;
            });

            break;
        default:
            return state;
    }
}