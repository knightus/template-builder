import { combineReducers } from 'redux';
// import { normalize, Schema, arrayOf } from 'normalizr';
import components from './components';
// import modals from './modals';

var log = require('bows/bows.js')('reducers/index');

const appReducer = combineReducers({
    components
    // modals
})

const rootReducer = (state, action) => {
    log('action: ', action.type, ' | ', action);
    if('LOAD_DATA' === action.type){
        state = action.payload.data;
    }

    return appReducer(state, action);
}

export default rootReducer;