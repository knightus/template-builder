'use strict';

//load the styles
require('./../../styles/style.less');

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider, connect} from 'react-redux';
// import {createStore, combineReducers, applyMiddleware} from 'redux';
import _ from 'lodash';

var log = require('bows/bows.js')('pages/index');

import configureStore from './../store';

//components
import Application from './../components/Application';
import Header from './../components/Header';
import Sidebar from './../components/Sidebar';
import Main from './../components/Main';

/**
 * load data samples
 */
import data1 from './../../data/1.js';
import data2 from './../../data/2.js';
import data3 from './../../data/3.js';
import data4 from './../../data/4.js';
import data5 from './../../data/5.js';
import data6 from './../../data/6.js';
import data7 from './../../data/7.js';

const data = data7;

import { normalize, Schema, arrayOf } from 'normalizr';
const template = new Schema('template');
const component = new Schema('components');
template.define({
    childComponents: arrayOf(component)
})
component.define({
    childComponents: arrayOf(component)
});
log(normalize(data, template));
const normalizedData = normalize(data, component).entities;
const store = configureStore(normalizedData);

/**
 * @function denormalize
 * @description use when saving
 */
function denormalize(state){
    const components = state.components;
    let template = components && components['1'] && _.cloneDeep(components['1']);
    let buildChildComponentsFromId = function(node){
        let newNode = _.cloneDeep(node);
        if(newNode.childComponents){
            let newChildComponents = _.map(newNode.childComponents, (childComponentId)=> {
                let newChildComponent = buildChildComponentsFromId(_.cloneDeep(components['' + childComponentId]));
                return newChildComponent;
            })
            newNode.childComponents = newChildComponents;
        }
        return newNode;
    }

    return buildChildComponentsFromId(template);
}

//start React
ReactDOM.render(
    <Provider store={store}>
        <Application>
            <Header></Header>
            <Sidebar></Sidebar>
            <Main></Main>
        </Application>
    </Provider>,
    wrapper
)

store.subscribe(function(){
    log('-------------');
    log('Current State');
    log(store.getState());
    log('-------------');
})

log('-------------');
log('Initial state');
log(store.getState());
log('-------------');

global.components = store.getState().components;
global._ = _;