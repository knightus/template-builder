import * as COMPONENT_TYPES from './component-types';

let componentsInfo = {}

componentsInfo[COMPONENT_TYPES.TEMPLATE] = {
    "displayName": "Template",
    "icon": "",
    "availableComponents": [COMPONENT_TYPES.CONTAINER]
}

componentsInfo[COMPONENT_TYPES.CONTAINER] = {
    "displayName": "Container",
    "icon": "",
    "availableComponents": [
        COMPONENT_TYPES.ROW
    ]
}

componentsInfo[COMPONENT_TYPES.ROW] = {
    "displayName": "Row",
    "availableComponents": [
        COMPONENT_TYPES.COLUMN
    ]
}

componentsInfo[COMPONENT_TYPES.COLUMN] = {
    "displayName": "Column",
    "availableComponents": _
        .chain(COMPONENT_TYPES)
        .values()
        .filter((item) => {
            return _.indexOf([
                COMPONENT_TYPES.TEMPLATE,
                COMPONENT_TYPES.COLUMN
            ], item)
        })
        .value()
}

componentsInfo[COMPONENT_TYPES.EMPTYSPACE] = {
    "displayName": "Empty Space",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.HORIZONTALTAB] = {
    "displayName": "Horizontal Tab",
    "availableComponents": [
        COMPONENT_TYPES.TAB_ITEM
    ]
}
componentsInfo[COMPONENT_TYPES.HORIZONTALLINE] = {
    "displayName": "Horizontal Line",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.BANNERSGROUP1] = {
    "displayName": "Banners Group #1",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.BANNERSGROUP2] = {
    "displayName": "Banners Group #2",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.PRODUCTSLIST] = {
    "displayName": "Products List",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.COMPLEX1] = {
    "displayName": "Complex #1",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.COMPLEX2] = {
    "displayName": "Complex #2",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.PARAGRAPH] = {
    "displayName": "Paragraph",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.TEXT] = {
    "displayName": "Text",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.BUTTON] = {
    "displayName": "Button",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.VIDEO] = {
    "displayName": "Video",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.VERTICALTAB] = {
    "displayName": "Vertical Tab",
    "availableComponents": [
        COMPONENT_TYPES.TAB_ITEM
    ]
}
componentsInfo[COMPONENT_TYPES.MENU] = {
    "displayName": "Menu",
    "availableComponents": []
}
componentsInfo[COMPONENT_TYPES.FILTER] = {
    "displayName": "Filter",
    "availableComponents": []
}

componentsInfo[COMPONENT_TYPES.TAB_ITEM] = {
    "displayName": "Tab Item",
    "availableComponents": _
        .chain(COMPONENT_TYPES)
        .values()
        .filter((item) => {
            return _.indexOf([
                COMPONENT_TYPES.TAB_ITEM
            ], item)
        })
        .value()
}

export default componentsInfo;