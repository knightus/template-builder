import React from 'react';
import { connect } from 'react-redux';

class Header extends React.Component {
    render(){
        return (
            <div className="tpl header" id="header">
                <div id="header__logo">
                    Template Builder
                </div>
                <div id="header__left">
                    <div className="list">
                        <a href="#" className="active item">
                            <i className="fa fa-angle-left"></i>
                            {" "}
                            Trang chủ
                        </a>
                    </div>
                </div>

                <div id="header__right">
                    <div className="list">
                        <a href="#" className="item">
                            <i className="fa fa-search-minus"></i>
                        </a>
                        <a href="#" className="item">
                            <i className="fa fa-rotate-left"></i>
                        </a>
                        <a href="#" className="item">
                            <i className="fa fa-rotate-right"></i>
                        </a>
                        <a href="#" className="item">
                            <i className="fa fa-mobile"></i>
                        </a>
                        <a href="#" className="item">
                            <i className="fa fa-play"></i>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = function(state){
    return state
}

const mapDispatchToProps = function(dispatch){
    return {

    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);