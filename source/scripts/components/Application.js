import React from 'react';
import { connect } from 'react-redux';


class Application extends React.Component {
    render(){
        return (
            <div id="application" className="state-sidebar-collapsed">
                {this.props.children}
            </div>
        )
    }
}

const mapStateToProps = function(state){
    return state
}

const mapDispatchToProps = function(dispatch){
    return {

    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Application);