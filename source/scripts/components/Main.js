import React from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';

import Component from './Component';

const Main = ({
    components
}) => {
    let templateComponent = _.filter(
        components,
        {
            type: 'Template'
        }
    )[0];

    if(!templateComponent){
        return (
            <div id="main">
                Empty template
            </div>
        )
    }

    return (
        <div id="main">
            <Component
                    key={templateComponent.id}
                    parentId={0}
                    {...templateComponent}
                    components={components}
            />
        </div>
    )
}

const mapStateToProps = function(state){
    return state
}

const mapDispatchToProps = function(dispatch){
    return {

    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main);