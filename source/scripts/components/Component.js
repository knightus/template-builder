import React from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';
const prettyData = require('pretty-data').pd;

import classNames from 'classnames';

import componentsInfo from './../constants/components-info';
import * as Actions from './../actions';

function getComponent(components, id){
    return components[id];
}

const getAvailableChildComponentTypes = function(type){
    let results = componentsInfo && componentsInfo[type] && componentsInfo[type].availableComponents;
    return results;
}

const Component = ({
    id,
    type,
    childComponents,
    components,
    properties,
    isCollapsed,

    handleCollapseToggleClick
}) => {
    let componentClassName = classNames({
        'tpl': true,
        'component': true,
        'collapsed': isCollapsed
    })
    return (
        <div className={componentClassName} data-type="template">
            <div className="header">
                <div className="right tools">
                    <div href="#" className="tpl dropdown button">
                        <div className="text">
                            <i className="fa fa-ellipsis-v"></i>
                        </div>
                        <div className="menu">
                            <a href="#" className="item">
                                <i className="fa fa-pencil"></i>
                                {" "}
                                Edit
                            </a>

                            <a href="#" className="item">
                                <i className="fa fa-plus"></i>
                                {" "}
                                New Child
                            </a>

                            <a href="#" className="item">
                                <i className="fa fa-clone"></i>
                                {" "}
                                Clone
                            </a>

                            <div className="divider"></div>

                            <a href="#" className="item">
                                <i className="fa fa-times"></i>
                                {" "}
                                Remove
                            </a>
                        </div>
                    </div>
                    <div className="tpl button" onClick={()=>{handleCollapseToggleClick(id)}}>
                        {(()=> {
                            if(isCollapsed){
                                return <i className="fa fa-caret-up"></i>;
                            } else {
                                return <i className="fa fa-caret-down"></i>;
                            }
                        })()}

                    </div>
                </div>
                <div className="title">
                    <i className="fa fa-list"></i>
                    {" "}
                    {type}
                    {" "}
                </div>
            </div>
            <div className="body">
                <div className="properties">
                    <pre>{prettyData.json(properties)}</pre>
                </div>
                <div className="children">
                    {
                        _.map(childComponents, (childComponentId, index) => {
                            var childComponent = getComponent(components, childComponentId);
                            if(childComponent){
                                return (
                                    <Component
                                        {...childComponent}
                                        key={childComponent.id}
                                        properties={childComponent.properties}
                                        components={components}
                                        // isCollapsed={true}

                                        renderChildren
                                        handleCollapseToggleClick={handleCollapseToggleClick}
                                    />
                                )
                            } else {
                                return '';
                            }
                        })
                    }
                </div>
            </div>
        </div>
    )
}



const mapStateToProps = function(state, ownProps){
    return Object.assign({}, state, {
        availableChildComponentTypes: getAvailableChildComponentTypes(ownProps.type)
        // isCollapsed: false
    });
}

const mapDispatchToProps = function(dispatch, ownProps){
    return {
        handleCollapseToggleClick: function(id){
            console.log(ownProps);
            dispatch(Actions.componentToggleCollapse(id));
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Component);