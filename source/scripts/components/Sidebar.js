import React from 'react';
import { connect } from 'react-redux';

class Sidebar extends React.Component {
    render(){
        return (
            <div className="tpl sidebar" id="sidebar">
                <div className="tpl widget" data-type="1">
                    <div className="header">
                        <h2 className="title">
                            How it works?
                        </h2>
                        <p><b>Introduction:</b> This application is built for building template. Users can add pre-defined components & clone/remove/edit settings by their own will.</p>
                        <p>For each component, the settings can be independent from its HTML structure. So theorically, this Application can be use for both personal& at-work use.</p>
                        <hr/>
                        <p><b>Settings:</b> The settings are divided into two categories: Presentational & Content.</p>
                        <p>Ex: A carousel will have presentational settings like <code>slide speed</code>, <code>autoPlay</code>, <code>showTitle</code> and content ones like its <code>images</code></p>
                        <hr/>
                        <p><b>Import/export data:</b> The application receives nested data, then normalize it while using. The exported data will be denormalized to its previous scheme</p>
                        <hr/>
                        <p>
                            <b>Technologies used:</b>
                        </p>
                        <ul>
                            <li style={{marginBottom: 2}}><span className="label label-danger">CSS</span> Bootstrap</li>
                            <li style={{marginBottom: 2}}><span className="label label-danger">CSS</span> Semantic-UI (for grammar structure)</li>

                            <li style={{marginBottom: 2}}><span className="label label-warning">JS</span> scaleApp (pages structure)</li>
                            <li style={{marginBottom: 2}}><span className="label label-warning">JS</span> ReactJS + Redux</li>

                            <li style={{marginBottom: 2}}><span className="label label-primary">Tool</span> Webpack</li>
                        </ul>
                        <hr/>
                        <p>
                            <b>Done:</b>
                        </p>
                        <ul>
                            <li>Collapse components</li>
                            <li>Normalize/denormalize data</li>
                        </ul>
                        <hr/>
                        <p>
                            <b>Todos:</b>
                        </p>
                        <ul>
                            <li>Write reducers for remain actions (reference in <code>constants/action-types</code>)</li>
                        </ul>
                    </div>

                    <div className="body">
                        <div className="list">
                            {/*<a href="#" className="item">
                             <i className="fa fa-list"></i>
                             {" "}
                             Header</a>
                             <a href="#" className="item">Slide</a>
                             <a href="#" className="item">Creative</a>
                             <a href="#" className="item">Lastest Work</a>*/}
                            {/*<div href="#" className="item">
                                <div className="tpl segment">
                                    <div className="title">How it works?</div>
                                    <small className="body">
                                        I'm build this application for personal & at-work use. This application will try to read/update a JSON object which store data about a specific theme/template.
                                        The input data can be nested.
                                    </small>
                                </div>
                            </div>*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = function(state){
    return state
}

const mapDispatchToProps = function(dispatch){
    return {

    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Sidebar);