import * as ACTION_TYPES from './../constants/action-types';

export function componentAddChildComponent(id, childType){
    return {
        type: ACTION_TYPES.COMPONENT_ADD_CHILD,
        payload: {
            id: id,
            childId: nextComponentId++,
            childType: childType
        }
    }
}

export function componentCloneChildComponent(id, position){
    return {
        type: ACTION_TYPES.COMPONENT_CLONE_CHILD,
        payload: {
            id: id,
            sourceChildPosition: position,
            childId: nextComponentId++
        }
    }
}

export function componentRemoveChildComponent(id, position){
    return {
        type: ACTION_TYPES.COMPONENT_REMOVE_CHILD,
        payload: {
            id: id,
            childPosition: position
        }
    }
}

export function componentRemove(id){
    return {
        type: ACTION_TYPES.COMPONENT_REMOVE,
        payload: {
            id: id
        }
    }
}

export function componentToggleCollapse(id){
    return {
        type: ACTION_TYPES.COMPONENT_TOGGLE_COLLAPSE,
        payload: {
            id: id
        }
    }
}

export function componentToggleCollapseAll(id){
    return {
        type: ACTION_TYPES.COMPONENT_TOGGLE_COLLAPSE_ALL
    }
}